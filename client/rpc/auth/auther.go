package auth

import (
	"gitee.com/Mr-zhaopei/keyauth/apps/audit"
	"gitee.com/Mr-zhaopei/keyauth/apps/policy"
	"gitee.com/Mr-zhaopei/keyauth/apps/token"
	"gitee.com/Mr-zhaopei/keyauth/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

//keyauth提供的HTTP认证中间件

func NewKeyauthAuther(client *rpc.ClientSet, serviceName string) *KeyauthAuther {
	return &KeyauthAuther{
		auth:        client.Token(),
		perm:        client.Policy(),
		audit:       client.Audit(),
		log:         zap.L().Named("http.auther"),
		serviceName: serviceName,
	}
}

type KeyauthAuther struct {
	log         logger.Logger
	auth        token.ServiceClient
	perm        policy.RPCClient
	audit       audit.RPCClient
	serviceName string
}

// func NewKeyauthAuther(client *rpc.ClientSet, serviceName string) *KeyauthAuther {
// 	return &KeyauthAuther{
// 		auth: client.Token(),
// 		// perm:        client.Policy(),
// 		// audit:       client.Audit(),
// 		log:         zap.L().Named("http.auther"),
// 		serviceName: serviceName,
// 	}
// }
