package rpc

import (
	"fmt"

	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitee.com/Mr-zhaopei/keyauth/apps/audit"
	"gitee.com/Mr-zhaopei/keyauth/apps/endpoint"
	"gitee.com/Mr-zhaopei/keyauth/apps/policy"
	"gitee.com/Mr-zhaopei/keyauth/apps/role"
	"gitee.com/Mr-zhaopei/keyauth/apps/token"
	"github.com/infraboard/mcenter/client/rpc"
	"github.com/infraboard/mcenter/client/rpc/auth"
	"github.com/infraboard/mcenter/client/rpc/resolver"
)

var (
	client *ClientSet
)

// SetGlobal todo
func SetGlobal(cli *ClientSet) {
	client = cli
}

// C Global
func C() *ClientSet {
	return client
}

// NewClient todo
func NewClient(conf *rpc.Config) (*ClientSet, error) {
	zap.DevelopmentSetup()
	log := zap.L()
	// resolver 进行解析的时候 需要mcenter客户端实例已经初始化
	log.Debugf("clientid: %s,client.Secret:%s", conf.ClientID, conf.ClientSecret)
	conn, err := grpc.Dial(
		// 127.0.0.1:18010 GRPC server端的地址
		// 基于服务发现  Dial to "passthrough://  dns://keyauth.org "mcenter://keyauth",
		fmt.Sprintf("%s://%s", resolver.Scheme, "keyauth"),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(auth.NewAuthentication(conf.ClientID, conf.ClientSecret)),
		grpc.WithBlock(),
	)

	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
		log:  log,
	}, nil
}

// Client 客户端
type ClientSet struct {
	conn *grpc.ClientConn
	log  logger.Logger
}

// Token服务的SDK
func (c *ClientSet) Token() token.ServiceClient {
	return token.NewServiceClient(c.conn)
}

// endpoint服务的SDK
func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}

// Role服务的SDK
func (c *ClientSet) Role() role.RPCClient {
	return role.NewRPCClient(c.conn)
}

// Policy服务的SDK
func (c *ClientSet) Policy() policy.RPCClient {
	return policy.NewRPCClient(c.conn)
}

// Audit服务的SDK
func (c *ClientSet) Audit() audit.RPCClient {
	return audit.NewRPCClient(c.conn)
}
