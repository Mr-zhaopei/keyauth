package impl

import (
	"context"

	"gitee.com/Mr-zhaopei/keyauth/apps/audit"
	"github.com/infraboard/mcube/exception"
)

//将客户操作的请求进行保存在mongodb记录中
func (s *service) AuditOperate(ctx context.Context, req *audit.OperateLog) (*audit.AuditOperateLogResponse, error) {
	if _, err := s.col.InsertOne(ctx, req); err != nil {
		return nil, exception.NewInternalServerError("inserted audit log(%s) document error,%s", req, err)
	}
	return &audit.AuditOperateLogResponse{}, nil
}
