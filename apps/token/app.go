package token

import (
	"fmt"
	"time"

	"gitee.com/Mr-zhaopei/keyauth/apps/user"
	"gitee.com/Mr-zhaopei/keyauth/common/utils"
	"github.com/infraboard/mcube/exception"
)

const (
	AppName = "token"
)

//token类型校验
func (req *IssueTokenRequest) Validate() error {
	switch req.GranteType {
	case GranteType_PASSWORD:
		if req.UserName == "" || req.Password == "" {
			return fmt.Errorf("password grant required username and password")
		}
	}
	return nil
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{
		UserDomain: user.DefaultDomain,
	}
}

//初始化默认的token
func NewDefaultToken() *Token {
	return &Token{
		Data: &IssueTokenRequest{},
		Meta: map[string]string{},
	}
}

func NewToken(req *IssueTokenRequest, expiredDuration time.Duration) *Token {
	now := time.Now()
	//token定义过期时间
	expired := now.Add(expiredDuration)
	refresh := now.Add(expiredDuration * 5)
	return &Token{
		AccessToken:           utils.MakeBearer(24),
		IssueAt:               time.Now().UnixMilli(),
		Data:                  req,
		AccessTokenExpiredAt:  expired.UnixMilli(),
		RefreshToken:          utils.MakeBearer(32),
		RefreshTokenExpiredAt: refresh.UnixMilli(),
	}
}

func (t *Token) Validate() error {
	//校验access token是否过期
	//是一个时间搓
	//now expire
	if time.Now().UnixMilli() > t.AccessTokenExpiredAt {
		//token不合法
		return exception.NewAccessTokenExpired("access token expired")
	}
	return nil
}

func (t *Token) IsRefreshTokenExpired() bool {
	//校验Refresh token是否过期
	//是一个时间搓
	//now expire
	if time.Now().UnixMilli() > t.RefreshTokenExpiredAt {
		//token不合法
		return true
	}
	return false
}

//续约Token，延长token的一个生命周期
func (t *Token) Extend(expiredDuration time.Duration) {
	now := time.Now()
	//token定义过期时间
	expired := now.Add(expiredDuration)
	refresh := now.Add(expiredDuration * 5)
	t.AccessTokenExpiredAt = expired.UnixMilli()
	t.RefreshTokenExpiredAt = refresh.UnixMilli()
}

func NewDescribeTokenRequest(at string) *DescribeTokenRequest {
	return &DescribeTokenRequest{
		AccessToken: at,
	}
}

func NewValidateTokenRequest(at string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: at,
	}
}

func NewRevolkTokenRequest() *RevolkTokenRequest {
	return &RevolkTokenRequest{}
}
