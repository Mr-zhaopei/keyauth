package impl

import (
	"context"
	"encoding/json"
	"fmt"

	"gitee.com/Mr-zhaopei/keyauth/apps/token"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Save Object
func (s *impl) save(ctx context.Context, ins *token.Token) error {
	// s.col.InsertMany()
	if _, err := s.col.InsertOne(ctx, ins); err != nil {
		return exception.NewInternalServerError("inserted token(%s) document error, %s",
			ins.AccessToken, err)
	}
	return nil
}

//token删除
func (s *impl) delete(ctx context.Context, ins *token.Token) error {
	if ins == nil || ins.AccessToken == "" {
		return fmt.Errorf("access token is nil")
	}
	// delete from access token where id = ?
	result, err := s.col.DeleteOne(ctx, bson.M{"_id": ins.AccessToken})
	if err != nil {
		return exception.NewInternalServerError("delete access token(%s) error, %s", ins.AccessToken, err)
	}

	//如果删除的结果显示为0
	if result.DeletedCount == 0 {
		return exception.NewNotFound("access token %s not found", ins.AccessToken)
	}

	//立即清除缓存
	delResult,err := s.redis.Del(ctx,ins.AccessToken).Result()
	if err != nil{
		s.log.Warnf("delete redis cache error,%s",err)
	} else {
		s.log.Debugf("%d",delResult)
	}
	return nil
}

//GET，describe token
//filter 过滤器(collection)，类似于MYSQL Where条件
//调用Decode方法来进行反序列化 bytes ----》 Object(通过BSON Tag)
func (s *impl) getFromDB(ctx context.Context, accessToken string) (*token.Token, error) {
	//1.通过bson过滤mongdb中的token
	filter := bson.M{"_id": accessToken}
	ins := token.NewDefaultToken()
	//根据找到的token和ins进行映射
	if err := s.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("access token %s not found", accessToken)
		}
		return nil, exception.NewInternalServerError("find access token %s error, %s", accessToken, err)
	}
	return ins, nil

}

//带有redis缓存逻辑
func (s *impl) get(ctx context.Context, accessToken string) (*token.Token, error) {

	//通过缓存redis进行获取redis
	resp, err := s.redis.Get(ctx, accessToken).Bytes()
	if err != nil {
		s.log.Warnf("redis get error,%s", err)
	} else {
		//初始化一个token结构体
		tk := token.NewDefaultToken()
		if err := json.Unmarshal(resp, tk); err != nil {
			s.log.Warnf("Unmarshal redis date error,%s", err)
		}
	}
	//直接从数据库中获取
	tk, err := s.getFromDB(ctx, accessToken)
	if err != nil {
		return nil, err
	}

	//设置缓存
	jtk, err := json.Marshal(tk)
	if err != nil {
		s.log.Warnf("Marshal token error,%s", err)
	}
	s.redis.Set(ctx, tk.AccessToken, string(jtk), 600)
	return tk, err
}

//UpdateById,通过主键来更新对象
func (s *impl) update(ctx context.Context, ins *token.Token) error {
	// SQL update obj(SET f=v,f=v) where id=?
	// s.col.UpdateOne(ctx, filter(), ins)
	if _, err := s.col.UpdateByID(ctx, ins.AccessToken, ins); err != nil {
		return exception.NewInternalServerError("update token(%s) document error,%s", ins.AccessToken, err)
	}
	return nil
}
