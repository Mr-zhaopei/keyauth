package role_test

import (
	"testing"

	"gitee.com/Mr-zhaopei/keyauth/apps/role"
)

func TestHasPermission(t *testing.T) {
	//初始化一个角色列表
	set := role.NewRoleSet()
	r := &role.Role{
		Spec: &role.CreateRoleRequest{
			Permissions: []*role.Permission{
				{
					Service:  "cmdb",
					AllowAll: true,
				},
			},
		},
	}
	set.Add(r)
	//权限判断
	perm, role := set.HasPermission(&role.PermissionRequest{
		Service:  "cmdb",
		Resource: "secret",
		Action:   "delete",
	})
	t.Log(role)
	if perm != true {
		t.Fatal("has perm error")
	}
}
