package endpoint

type Service interface {
	RPCServer
}

//将所有的endpoint集合转换为列表中
func (s *EndpointSet) ToDocs() (docs []interface{}) {
	for i := range s.Endpoints {
		docs = append(docs, s.Endpoints[i])
	}
	return
}

//构建一个相应消息
func NewRegistryResponse() *RegistryResponse {
	return &RegistryResponse{}
}
