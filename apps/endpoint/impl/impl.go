package impl

import (
	"gitee.com/Mr-zhaopei/keyauth/apps/endpoint"
	"gitee.com/Mr-zhaopei/keyauth/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	//服务实例
	svr = &service{}
)

type service struct {
	col *mongo.Collection
	log logger.Logger
	endpoint.UnimplementedRPCServer
}

//配置刷新
func (s *service) Config() error {
	//依赖Mongodb的DB对象
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	//获取一个Collection对象，通过Collection对象来进行CRUD
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())
	return nil
}

func (s *service) Name() string {
	return endpoint.AppName
}

func (s *service) Registry(server *grpc.Server) {
	endpoint.RegisterRPCServer(server, svr)
}

func init() {
	//内部服务注册
	app.RegistryInternalApp(svr)
	//GRPC服务注册
	app.RegistryGrpcApp(svr)
}
