package impl

import (
	"context"

	"gitee.com/Mr-zhaopei/keyauth/apps/endpoint"
	"github.com/infraboard/mcube/exception"
)

//对象保存
func (s *service) save(ctx context.Context, set *endpoint.EndpointSet) error {
	if _, err := s.col.InsertMany(ctx, set.ToDocs()); err != nil {
		return exception.NewInternalServerError("insert service %s endpoint document error,%s", set.Service, err)
	}
	return nil
}
