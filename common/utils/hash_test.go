package utils_test

import (
	"testing"

	"gitee.com/Mr-zhaopei/keyauth/common/utils"
)

func TestHash(t *testing.T) {
	v := utils.Hash("abc")
	t.Log(v)
}

//$2a$14$LeEXCSl4iQbhQxAYUC5JwuizDur1.VmiSCrFXjpTdHmF8ECZs23sO
func TestHashPassword(t *testing.T) {
	v := utils.HashPassword("abc")
	t.Log(v)
	ok := utils.CheckPasswordHash("abc", "$2a$14$LeEXCSl4iQbhQxAYUC5JwuizDur1.VmiSCrFXjpTdHmF8ECZs23sO")
	t.Log(ok)

}
